package net.allegea.grepolis.dao;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import net.allegea.grepolis.dao.exception.DAOException;

public class AbstractURLDAO extends AbstractDAO {

	public AbstractURLDAO(String filePath) {
		super(filePath);
	}
	
	public BufferedReader getBufferedReader() {
		BufferedReader buffer = null;
		if(bufferCache.containsKey(getFilePath())) {
			InputStream inputStream = new ByteArrayInputStream(bufferCache.get(getFilePath()).getBytes());
			buffer = new BufferedReader(new InputStreamReader(inputStream));
		}
		else {
			try {
				URL url = new URL(getFilePath());
				BufferedReader temporaryBuffer = new BufferedReader(new InputStreamReader(url.openStream()));
				
				StringBuffer stringBuffer = new StringBuffer();
				String line = null;
				while((line = temporaryBuffer.readLine()) != null) {
					stringBuffer.append(line);
					stringBuffer.append(System.getProperty("line.separator"));
				}
				temporaryBuffer.close();
				bufferCache.put(getFilePath(), stringBuffer.toString());
				
				InputStream inputStream = new ByteArrayInputStream(stringBuffer.toString().getBytes());
				buffer = new BufferedReader(new InputStreamReader(inputStream));
			}
			catch (MalformedURLException e) {
				throw new DAOException(e);
			} 
			catch (IOException e) {
				throw new DAOException(e);
			}
		}
		
		return buffer;
	}

}
