package net.allegea.grepolis.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AbstractDAO {

	private String filePath;

	protected static Map<String, String> bufferCache = new HashMap<String, String>();
	
	public String getFilePath() {
		return filePath;
	}

	public AbstractDAO(String filePath) {
		super();
		this.filePath = filePath;
	}
	
	protected String getString(String value) {
		return value;
	}
	
	protected int getInt(String value) {
		int intValue = 0;
		try {
			intValue = new Integer(value).intValue();
		}
		catch(Exception e) {
			
		}
	
		return intValue;
	}
	
	protected long getLongt(String value) {
		long longValue = 0;
		try {
			longValue = new Long(value).longValue();
		}
		catch(Exception e) {
			
		}
	
		return longValue;
	}

	protected Date getDate(String value) {
		return new Date(new Long(value).longValue() * 1000);
	}
	
}
