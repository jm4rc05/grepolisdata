package net.allegea.grepolis.dao;

import net.allegea.grepolis.api.Version;

public interface DAOFactory<D> {

	public D getInstance(String filePath);

	public D getInstance(String filePath, Version version);
	
}
