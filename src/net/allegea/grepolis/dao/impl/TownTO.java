package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Town;

public class TownTO implements Town {

	private int id;
	
	private int playerId;
	
	private String name;
	
	private int islandX;
	
	private int islandY;
	
	private int numberOnIsland;
	
	private int points;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int getPlayerId() {
		return playerId;
	}

	@Override
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getIslandX() {
		return islandX;
	}

	@Override
	public void setIslandX(int islandX) {
		this.islandX = islandX;
	}

	@Override
	public int getIslandY() {
		return islandY;
	}

	@Override
	public void setIslandY(int islandY) {
		this.islandY = islandY;
	}
	
	@Override
	public int getNumberOnIsland() {
		return numberOnIsland;
	}

	@Override
	public void setNumberOnIsland(int numberOnIsland) {
		this.numberOnIsland = numberOnIsland;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;
	}

	public TownTO(int id, int playerId, String name, int islandX, int islandY, int numberOnIsland, int points) {
		super();
		this.id = id;
		this.playerId = playerId;
		this.name = name;
		this.islandX = islandX;
		this.islandY = islandY;
		this.numberOnIsland = numberOnIsland;
		this.points = points;
	}

	@Override
	public String toString() {
		return "TownTO [id=" + id + ", playerId=" + playerId + ", name=" + name + ", islandX=" + islandX + ", islandY=" + islandY + ", numberOnIsland=" + numberOnIsland + ", points=" + points + "]";
	}
	
}
