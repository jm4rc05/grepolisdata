package net.allegea.grepolis.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import net.allegea.grepolis.api.Alliance;
import net.allegea.grepolis.dao.AbstractURLDAO;
import net.allegea.grepolis.dao.exception.DAOException;

public class AllianceDAOImpl extends AbstractURLDAO implements AllianceDAO {
	
	@Override
	public Alliance getAlliance(int id) {
		Alliance alliance = null;
		try {
			BufferedReader buffer = getBufferedReader();
			while((alliance = processLine(buffer.readLine())) != null) {
				if(alliance.getId() == id) {
					break;
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return alliance;
	}
	
	private Alliance processLine(String line) {
		Alliance alliance = null;
		Scanner scanner = new Scanner(line);
		scanner.useDelimiter(",");
		if(scanner.hasNext()) {
			int id = getInt(scanner.next());
			String name = getString(scanner.next());
			int points = getInt(scanner.next());
			int towns = getInt(scanner.next());
			int members = getInt(scanner.next());
			int rank = getInt(scanner.next());
			
			alliance = new AllianceTO(id, name, points, towns, members, rank);
		}
		scanner.close();
		
		return alliance;
	}
	
	public AllianceDAOImpl(String filePath) {
		super(filePath);
	}

}
