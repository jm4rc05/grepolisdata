package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Alliance;

public class AllianceTO implements Alliance {

	private int id;
	
	private String name;
	
	private int points;
	
	private int towns;

	private int members;
	
	private int rank;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public int getTowns() {
		return towns;
	}

	@Override
	public void setTowns(int towns) {
		this.towns = towns;
	}

	@Override
	public int getMembers() {
		return members;
	}

	@Override
	public void setMembers(int members) {
		this.members = members;
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public void setRank(int rank) {
		this.rank = rank;
	}

	public AllianceTO(int id, String name, int points, int towns, int members, int rank) {
		this.id = id;
		this.name = name;
		this.points = points;
		this.towns = towns;
		this.members = members;
		this.rank = rank;
	}
	
	@Override
	public String toString() {
		return "Alliance [id=" + id + ", name=" + name + ", points=" + points + ", towns=" + towns + ", members=" + members + ", rank=" + rank + "]";
	}
	
}
