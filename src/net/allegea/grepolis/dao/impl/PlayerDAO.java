package net.allegea.grepolis.dao.impl;

import java.util.List;

import net.allegea.grepolis.api.Player;

public interface PlayerDAO {

	public abstract Player getPlayer(int id);

	public abstract List<Player> getPlayersInRange(int id, int range);
	
}
