package net.allegea.grepolis.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import net.allegea.grepolis.api.Conquest;
import net.allegea.grepolis.dao.AbstractURLDAO;
import net.allegea.grepolis.dao.exception.DAOException;


public class ConquestDAOImpl extends AbstractURLDAO implements ConquestDAO {

	@Override
	public List<Conquest> getConquestsByNewPlayerId(int playerId) {
		List<Conquest> list = new ArrayList<Conquest>();
		try {
			BufferedReader buffer = getBufferedReader();
			Conquest conquest = null;
			while((conquest = processLine(buffer.readLine())) != null) {
				if(conquest.getNewOwnerId() == playerId) {
					list.add(conquest);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

	@Override
	public List<Conquest> getConquestsByNewAlliance(int allianceId) {
		List<Conquest> list = new ArrayList<Conquest>();
		try {
			BufferedReader buffer = getBufferedReader();
			Conquest conquest = null;
			while((conquest = processLine(buffer.readLine())) != null) {
				if(conquest.getNewAllianceId() == allianceId) {
					list.add(conquest);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

	@Override
	public List<Conquest> getConquestsByOldPlayerId(int playerId) {
		List<Conquest> list = new ArrayList<Conquest>();
		try {
			BufferedReader buffer = getBufferedReader();
			Conquest conquest = null;
			while((conquest = processLine(buffer.readLine())) != null) {
				if(conquest.getOldOwnerId() == playerId) {
					list.add(conquest);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

	@Override
	public List<Conquest> getConquestsByOldAllianceId(int allianceId) {
		List<Conquest> list = new ArrayList<Conquest>();
		try {
			BufferedReader buffer = getBufferedReader();
			Conquest conquest = null;
			while((conquest = processLine(buffer.readLine())) != null) {
				if(conquest.getOldAllianceId() == allianceId) {
					list.add(conquest);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

	@Override
	public List<Conquest> getConquestsByDateRange(Date from, Date to) {
		List<Conquest> list = new ArrayList<Conquest>();
		try {
			BufferedReader buffer = getBufferedReader();
			Conquest conquest = null;
			while((conquest = processLine(buffer.readLine())) != null) {
				if(conquest.getDate().after(from) && conquest.getDate().before(to)) {
					list.add(conquest);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}
	
	private Conquest processLine(String line) {
		Conquest conquest = null;
		if(line != null) {
			Scanner scanner = new Scanner(line);
			scanner.useDelimiter(",");
			if(scanner.hasNext()) {
				int townId = getInt(scanner.next());
				Date date = getDate(scanner.next());
				int newOwnerId = getInt(scanner.next());
				int oldOwnerId = getInt(scanner.next());
				int newAllianceId = getInt(scanner.next());
				int oldAllianceId = getInt(scanner.next());
				int points = getInt(scanner.next());
				
				conquest = new ConquestTO(townId, date, newOwnerId, newAllianceId, oldOwnerId, oldAllianceId, points);
			}
			scanner.close();
		}
		
		return conquest;
	}

	public ConquestDAOImpl(String filePath) {
		super(filePath);
	}

}
