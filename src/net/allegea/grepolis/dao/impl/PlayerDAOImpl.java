package net.allegea.grepolis.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import net.allegea.grepolis.api.Player;
import net.allegea.grepolis.dao.AbstractURLDAO;
import net.allegea.grepolis.dao.exception.DAOException;

public class PlayerDAOImpl extends AbstractURLDAO implements PlayerDAO {
	
	@Override
	public Player getPlayer(int id) {
		Player player = null;
		try {
			BufferedReader buffer = getBufferedReader();
			while((player = processLine(buffer.readLine())) != null) {
				if(player.getId() == id) {
					break;
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return player;
	}

	@Override
	public List<Player> getPlayersInRange(int id, int range) {

		return null;
	}
	
	private Player processLine(String line) {
		Player player = null;
		Scanner scanner = new Scanner(line);
		scanner.useDelimiter(",");
		if(scanner.hasNext()) {
			int id = getInt(scanner.next());
			String name = getString(scanner.next());
			int allianceId = getInt(scanner.next());
			int points = getInt(scanner.next());
			int rank = getInt(scanner.next());
			int towns = getInt(scanner.next());
			
			player = new PlayerTO(id, name, allianceId, points, rank, towns);
		}
		scanner.close();
		
		return player;
	}

	public PlayerDAOImpl(String filePath) {
		super(filePath);
	}
	
}
