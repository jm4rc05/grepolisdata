package net.allegea.grepolis.dao.impl;

import java.lang.reflect.InvocationTargetException;

import net.allegea.grepolis.api.Version;
import net.allegea.grepolis.dao.DAOFactory;
import net.allegea.grepolis.dao.exception.DAOException;

public class AllianceDAOFactory implements DAOFactory<AllianceDAO> {
	
	private static Class<?>[] classes = {
		AllianceDAOImpl.class
	};
	
	@Override
	public AllianceDAO getInstance(String filePath) {
		return getInstance(filePath, Version.V1R0);
	}
	
	@Override
	public AllianceDAO getInstance(String filePath, Version version) {
		if(version.ordinal() < 0 && version.ordinal() > classes.length) {
			throw new DAOException(new IllegalArgumentException("Version " + version.toString() + " not implemented.")); 
		}
		try {
			return (AllianceDAO) classes[version.ordinal()].getDeclaredConstructor(String.class).newInstance(filePath);
		}
		catch(InstantiationException e) {
			throw new DAOException(e);
		}
		catch(IllegalAccessException e) {
			throw new DAOException(e);
		} 
		catch (InvocationTargetException e) {
			throw new DAOException(e);
		} 
		catch (NoSuchMethodException e) {
			throw new DAOException(e);
		} 
	}

}
