package net.allegea.grepolis.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import net.allegea.grepolis.api.Kill;
import net.allegea.grepolis.dao.AbstractURLDAO;
import net.allegea.grepolis.dao.exception.DAOException;

public class KillDAOImpl extends AbstractURLDAO implements KillDAO {
	
	@Override
	public Kill getKill(int id) {
		Kill kill = null;
		try {
			BufferedReader buffer = getBufferedReader();
			while((kill = processLine(buffer.readLine())) != null) {
				if(kill.getId() == id) {
					break;
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return kill;
	}

	private Kill processLine(String line) {
		Kill kill = null;
		if(line != null) {
			Scanner scanner = new Scanner(line);
			scanner.useDelimiter(",");
			if(scanner.hasNext()) {
				int position = getInt(scanner.next());
				int id = getInt(scanner.next());
				int points = getInt(scanner.next());
				
				kill = new KillTO(id, position, points);
			}
			scanner.close();
		}
		
		return kill;
	}
	
	public KillDAOImpl(String filePath) {
		super(filePath);
	}
	
}
