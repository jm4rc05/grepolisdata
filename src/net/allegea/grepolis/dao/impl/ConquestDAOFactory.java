package net.allegea.grepolis.dao.impl;

import java.lang.reflect.InvocationTargetException;

import net.allegea.grepolis.api.Version;
import net.allegea.grepolis.dao.DAOFactory;
import net.allegea.grepolis.dao.exception.DAOException;

public class ConquestDAOFactory implements DAOFactory<ConquestDAO> {

	private static Class<?>[] classes = {
		ConquestDAOImpl.class
	};
	
	@Override
	public ConquestDAO getInstance(String filePath) {
		return getInstance(filePath, Version.V1R0);
	}
	
	@Override
	public ConquestDAO getInstance(String filePath, Version version) {
		if(version.ordinal() < 0 && version.ordinal() > classes.length) {
			throw new DAOException(new IllegalArgumentException("Version " + version.toString() + " not implemented."));
		}
		try {
			return (ConquestDAO) classes[version.ordinal()].getDeclaredConstructor(String.class).newInstance(filePath);
		}
		catch(InstantiationException e) {
			throw new DAOException(e);
		}
		catch(IllegalAccessException e) {
			throw new DAOException(e);
		} 
		catch (InvocationTargetException e) {
			throw new DAOException(e);
		} 
		catch (NoSuchMethodException e) {
			throw new DAOException(e);
		} 
	}
	
}
