package net.allegea.grepolis.dao.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import net.allegea.grepolis.api.Player;
import net.allegea.grepolis.api.Town;
import net.allegea.grepolis.dao.AbstractURLDAO;
import net.allegea.grepolis.dao.exception.DAOException;
import net.allegea.grepolis.geography.Area;
import net.allegea.grepolis.geography.Location;
import net.allegea.grepolis.geography.Ocean;

public class TownDAOImpl extends AbstractURLDAO implements TownDAO {

	@Override
	public Town getTown(int id) {
		Town town = null;
		try {
			BufferedReader buffer = getBufferedReader();
			while((town = processLine(buffer.readLine())) != null) {
				if(town.getId() == id) {
					break;
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return town;
	}
	
	@Override
	public List<Town> getOceanTowns(int id) {
		Ocean ocean = new Ocean(id);
		List<Town> list = new ArrayList<Town>();
		try {
			BufferedReader buffer = getBufferedReader();
			Town town = null;
			while((town = processLine(buffer.readLine())) != null) {
				Location townLocation = new Location(town.getIslandX(), town.getIslandY());
				if(ocean.isLocationInOcean(townLocation)) {
					list.add(town);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

	@Override
	public Town getNearestTown(int id, Player player) {
		List <Town> playerTowns = getPlayerTowns(player.getId());
		Town nearestTown = null;
		if(playerTowns != null) {
			int shortestDistance = 0;
			Town refTown = getTown(id);
			for (Town town : playerTowns) {
				Location base = new Location(refTown.getIslandX(), refTown.getIslandY());
				Location currentLocation = new Location(town.getIslandX(), town.getIslandY());
				int currentDistance = currentLocation.getDistance(base);
				if(shortestDistance > 0 && shortestDistance < currentDistance) {
					nearestTown = town;
					shortestDistance = currentDistance;
				}
			}
		}
		
		return nearestTown;
	}

	@Override
	public List<Town> getTownsInRange(int id, int range) {
		Town refTown = getTown(id);
		Location refTownLocation = new Location(refTown.getIslandX(), refTown.getIslandY());
		Area area = new Area(refTownLocation, range);
		List<Town> list = new ArrayList<Town>();
		try {
			BufferedReader buffer = getBufferedReader();
			Town town = null;
			while((town = processLine(buffer.readLine())) != null) {
				Location townLocation = new Location(town.getIslandX(), town.getIslandY());
				if(area.isLocationInArea(townLocation)) {
					list.add(town);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}
	
	@Override
	public List<Town> getPlayerTowns(int id) {
		List<Town> list = new ArrayList<Town>();
		try {
			BufferedReader buffer = getBufferedReader();
			Town town = null;
			while((town = processLine(buffer.readLine())) != null) {
				if(town.getPlayerId() == id) {
					list.add(town);
				}
			}
		}
		catch (IOException e) {
			throw new DAOException(e);
		}
		
		return list;
	}
	
	private Town processLine(String line) {
		Town town = null;
		if(line != null) {
			Scanner scanner = new Scanner(line);
			scanner.useDelimiter(",");
			if(scanner.hasNext()) {
				int id = getInt(scanner.next());
				int playerId = getInt(scanner.next());
				String name = getString(scanner.next());
				int islandX = getInt(scanner.next());
				int islandY = getInt(scanner.next());
				int numberOnIsland = getInt(scanner.next());
				int points = getInt(scanner.next());
				
				town = new TownTO(id, playerId, name, islandX, islandY, numberOnIsland, points);
			}
			scanner.close();
		}
		
		return town;
	}
	
	public TownDAOImpl(String filePath) {
		super(filePath);
	}

}
