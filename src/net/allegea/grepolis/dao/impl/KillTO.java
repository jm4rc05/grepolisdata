package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Kill;

public class KillTO implements Kill {

	private int id;

	private int position;
	
	private int points;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public KillTO(int id, int position, int points) {
		super();
		this.id = id;
		this.position = position;
		this.points = points;
	}
	
	@Override
	public String toString() {
		return "KillTO [id=" + id + ", position=" + position + ", points=" + points +" ]";
	}
	
}
