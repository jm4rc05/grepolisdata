package net.allegea.grepolis.dao.impl;

import java.util.List;

import net.allegea.grepolis.api.Player;
import net.allegea.grepolis.api.Town;


public interface TownDAO {

	public abstract Town getTown(int id);
	
	public abstract List<Town> getOceanTowns(int id);
	
	public abstract Town getNearestTown(int id, Player player);
	
	public abstract List<Town> getTownsInRange(int id, int range);
	
	public abstract List<Town> getPlayerTowns(int id);
	
}
