package net.allegea.grepolis.dao.impl;

import java.util.Date;
import java.util.List;

import net.allegea.grepolis.api.Conquest;


public interface ConquestDAO {

	public abstract List<Conquest> getConquestsByNewPlayerId(int playerId);
	
	public abstract List<Conquest> getConquestsByNewAlliance(int allianceId);
	
	public abstract List<Conquest> getConquestsByOldPlayerId(int playerId);
	
	public abstract List<Conquest> getConquestsByOldAllianceId(int allianceId);
	
	public abstract List<Conquest> getConquestsByDateRange(Date from, Date to);
	
}