package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Alliance;

public interface AllianceDAO {
	
	public abstract Alliance getAlliance(int id);

}
