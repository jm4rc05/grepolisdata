package net.allegea.grepolis.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import net.allegea.grepolis.api.Conquest;


public class ConquestTO implements Conquest {

	private int townId;
	
	private Date date;
	
	private int newOwnerId;
	
	private int newAllianceId;
	
	private int oldOwnerId;
	
	private int oldAllianceId;
	
	private int points;

	@Override
	public int getTownId() {
		return townId;
	}

	@Override
	public void setTownId(int townId) {
		this.townId = townId;
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int getNewOwnerId() {
		return newOwnerId;
	}

	@Override
	public void setNewOwnerId(int newOwnerId) {
		this.newOwnerId = newOwnerId;
	}

	@Override
	public int getNewAllianceId() {
		return newAllianceId;
	}

	@Override
	public void setNewAllianceId(int newAllianceId) {
		this.newAllianceId = newAllianceId;
	}

	@Override
	public int getOldOwnerId() {
		return oldOwnerId;
	}

	@Override
	public void setOldOwnerId(int oldOwnerId) {
		this.oldOwnerId = oldOwnerId;
	}

	@Override
	public int getOldAllianceId() {
		return oldAllianceId;
	}

	@Override
	public void setOldAllianceId(int oldAllianceId) {
		this.oldAllianceId = oldAllianceId;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;
	}

	public ConquestTO(int townId, Date date, int newOwnerId, int newAllianceId, int oldOwnerId, int oldAllianceId, int points) {
		super();
		this.townId = townId;
		this.date = date;
		this.newOwnerId = newOwnerId;
		this.newAllianceId = newAllianceId;
		this.oldOwnerId = oldOwnerId;
		this.oldAllianceId = oldAllianceId;
		this.points = points;
	}
	
	@Override
	public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
        TimeZone tz = TimeZone.getTimeZone("UTC");
        
        df.setTimeZone(tz);
        String formatedDate = df.format(date);
        
		return "Conquest [townId=" + townId + ", date=" + formatedDate + ", newOwnerId=" + newOwnerId + ", newAllianceId=" + newAllianceId + ", oldOwnerId=" + oldOwnerId + ", oldAllianceId=" + oldAllianceId + ", points=" + points;
	}
	
}
