package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Kill;

public interface KillDAO {
	
	public abstract Kill getKill(int id);
	
}