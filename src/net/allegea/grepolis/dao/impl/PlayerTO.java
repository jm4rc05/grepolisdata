package net.allegea.grepolis.dao.impl;

import net.allegea.grepolis.api.Player;


public class PlayerTO implements Player {

	private int id;
	
	private String name;
	
	private int allianceId;
	
	private int points;
	
	private int rank;
	
	private int towns;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getAllianceId() {
		return allianceId;
	}

	@Override
	public void setAllianceId(int allianceId) {
		this.allianceId = allianceId;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public void setRank(int rank) {
		this.rank = rank;
	}

	@Override
	public int getTowns() {
		return towns;
	}

	@Override
	public void setTowns(int towns) {
		this.towns = towns;
	}

	public PlayerTO(int id, String name, int allianceId, int points, int rank, int towns) {
		super();
		this.id = id;
		this.name = name;
		this.allianceId = allianceId;
		this.points = points;
		this.rank = rank;
		this.towns = towns;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", allianceId=" + allianceId + ", points=" + points + ", rank=" + rank + ", towns=" + towns + "]";
	}
	
}
