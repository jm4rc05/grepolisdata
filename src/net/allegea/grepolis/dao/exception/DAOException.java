package net.allegea.grepolis.dao.exception;


public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 9195161217622455272L;

	public DAOException(Exception e) {
		super(e);
		System.out.println(e);
	}

}
