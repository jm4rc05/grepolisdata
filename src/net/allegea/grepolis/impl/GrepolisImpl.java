package net.allegea.grepolis.impl;

import java.util.Date;
import java.util.List;

import net.allegea.grepolis.api.Alliance;
import net.allegea.grepolis.api.Conquest;
import net.allegea.grepolis.api.Grepolis;
import net.allegea.grepolis.api.Kill;
import net.allegea.grepolis.api.Player;
import net.allegea.grepolis.api.Town;
import net.allegea.grepolis.api.Version;
import net.allegea.grepolis.dao.impl.AllianceDAO;
import net.allegea.grepolis.dao.impl.AllianceDAOFactory;
import net.allegea.grepolis.dao.impl.ConquestDAO;
import net.allegea.grepolis.dao.impl.ConquestDAOFactory;
import net.allegea.grepolis.dao.impl.KillDAO;
import net.allegea.grepolis.dao.impl.KillDAOFactory;
import net.allegea.grepolis.dao.impl.PlayerDAO;
import net.allegea.grepolis.dao.impl.PlayerDAOFactory;
import net.allegea.grepolis.dao.impl.TownDAO;
import net.allegea.grepolis.dao.impl.TownDAOFactory;


public class GrepolisImpl implements Grepolis {

	private Version version = null;

	protected Version getVersion() {
		return this.version;
	}
	
	public GrepolisImpl(Version version) {
		super();
		this.version = version;
	}

	@Override
	public Player getPlayer(String filePath, int id) {
		PlayerDAO playerDAO = new PlayerDAOFactory().getInstance(filePath, getVersion());
		
		return playerDAO.getPlayer(id);
	}

	@Override
	public Alliance getAlliance(String filePath, int id) {
		AllianceDAO allianceDAO = new AllianceDAOFactory().getInstance(filePath, getVersion());
		
		return allianceDAO.getAlliance(id);
	}
	
	@Override
	public Town getTown(String filePath, int id) {
		TownDAO townDAO = new TownDAOFactory().getInstance(filePath, getVersion());
		
		return townDAO.getTown(id);
	}

	@Override
	public List<Town> getPlayerTowns(String filePath, int id) {
		TownDAO townDAO = new TownDAOFactory().getInstance(filePath, getVersion());
		
		return townDAO.getPlayerTowns(id);
	}
	
	@Override
	public List<Player> getPlayersInRange(String filePath, int idTown, int range) {
		PlayerDAO playerDAO = new PlayerDAOFactory().getInstance(filePath, getVersion());

		return playerDAO.getPlayersInRange(idTown, range);		
	}

	@Override
	public List<Town> getOceanTowns(String filePath, int id) {
		TownDAO townDAO = new TownDAOFactory().getInstance(filePath, getVersion());
		
		return townDAO.getOceanTowns(id);
	}
	
	@Override
	public List<Town> getTownsInRange(String filePath, int id, int range) {
		TownDAO townDAO = new TownDAOFactory().getInstance(filePath, getVersion());
		
		return townDAO.getTownsInRange(id, range);		
	}
	
	@Override
	public List<Conquest> getConquestsByNewPlayerId(String filePath, int playerId) {
		ConquestDAO conquestDAO = new ConquestDAOFactory().getInstance(filePath, getVersion());
		
		return conquestDAO.getConquestsByNewPlayerId(playerId);
	}

	@Override
	public List<Conquest> getConquestsByNewAlliance(String filePath, int allianceId) {
		ConquestDAO conquestDAO = new ConquestDAOFactory().getInstance(filePath, getVersion());
		
		return conquestDAO.getConquestsByNewAlliance(allianceId);
	}

	@Override
	public List<Conquest> getConquestsByOldPlayerId(String filePath, int playerId) {
		ConquestDAO conquestDAO = new ConquestDAOFactory().getInstance(filePath, getVersion());
		
		return conquestDAO.getConquestsByOldPlayerId(playerId);
	}

	@Override
	public List<Conquest> getConquestsByOldAllianceId(String filePath, int allianceId) {
		ConquestDAO conquestDAO = new ConquestDAOFactory().getInstance(filePath, getVersion());
		
		return conquestDAO.getConquestsByOldAllianceId(allianceId);
	}

	@Override
	public List<Conquest> getConquestsByDateRange(String filePath, Date from, Date to) {
		ConquestDAO conquestDAO = new ConquestDAOFactory().getInstance(filePath, getVersion());
		
		return conquestDAO.getConquestsByDateRange(from, to);
	}

	@Override
	public Kill getKill(String filePath, int id) {
		KillDAO killDAO = new KillDAOFactory().getInstance(filePath, getVersion());
		
		return killDAO.getKill(id);
	}
	
}
