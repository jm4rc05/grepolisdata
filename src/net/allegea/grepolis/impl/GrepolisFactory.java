package net.allegea.grepolis.impl;

import java.lang.reflect.InvocationTargetException;

import net.allegea.grepolis.api.Grepolis;
import net.allegea.grepolis.api.Version;


public class GrepolisFactory {

	public static final Version DEFAULT_VERSION = Version.V1R0;
	
	private static Class<?>[] classes = {
		GrepolisImpl.class
	};
	
	public final Grepolis getInstance() {
		return getInstance(DEFAULT_VERSION);
	}
	
	public final Grepolis getInstance(Version version) {
		if(version.ordinal() < 0 && version.ordinal() > classes.length) {
			throw new IllegalArgumentException("Version " + version.toString() + " not implemented.");
		}
		try {
			return (Grepolis) classes[version.ordinal()].getDeclaredConstructor(Version.class).newInstance(version);
		}
		catch(InstantiationException e) {
			throw new IllegalArgumentException(e);
		} 
		catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		} 
		catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(e);
		} 
		catch (InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		} 
		catch (NoSuchMethodException e) {
			throw new IllegalArgumentException(e);
		} 
		catch (SecurityException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
}
