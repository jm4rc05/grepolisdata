package net.allegea.grepolis.geography;


public class Ocean {

	private Location minLocation = new Location(0, 0);
	
	private Location maxLocation = new Location(99, 99);
	
	public Ocean(int id) {
		super();
		this.minLocation.setPositionX((id / 10) * 100);
		this.maxLocation.setPositionX(this.minLocation.getPositionX() + 99);
		this.minLocation.setPositionY((id - ((id / 10) * 10)) * 100);
		this.maxLocation.setPositionY(this.minLocation.getPositionY() + 99);
	}

	public Location getMinLocation() {
		return minLocation;
	}

	public Location getMaxLocation() {
		return maxLocation;
	}
	
	public boolean isLocationInOcean(Location location) {
		boolean insideMinBorderX = location.getPositionX() >= this.minLocation.getPositionX();
		boolean insideMaxBorderX = location.getPositionX() <= this.maxLocation.getPositionX();
		boolean insideMinBorderY = location.getPositionY() >= this.minLocation.getPositionY();
		boolean insideMaxBorderY = location.getPositionY() <= this.maxLocation.getPositionY();
		
		if(insideMinBorderX && insideMaxBorderX && insideMinBorderY && insideMaxBorderY) {
			return true;
		}
		else {
			return false;
		}
	}

}
