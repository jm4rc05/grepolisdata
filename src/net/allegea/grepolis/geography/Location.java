package net.allegea.grepolis.geography;


public class Location {

	private int positionX = 0;
	
	private int positionY = 0;
	
	public Location(int positionX, int positionY) {
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public int getPositionX() {
		return positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	
	public int getDistance(Location from) {
		int faceX = Math.abs(from.getPositionX() - this.positionX);
		int faceY = Math.abs(from.getPositionY() - this.positionY);
		
		int distance = new Double(Math.sqrt(Math.pow(faceX, 2) + Math.pow(faceY, 2))).intValue();
		
		return distance;
	}
	
}
