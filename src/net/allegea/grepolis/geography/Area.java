package net.allegea.grepolis.geography;


public class Area {

	private Location center = new Location(50, 50);
	
	private int radio = 50;
	
	public Area(Location center, int radio) {
		this.center = center;
		this.radio = radio;
	}

	public Location getCenter() {
		return center;
	}

	public int getRadio() {
		return radio;
	}
	
	public boolean isLocationInArea(Location location) {
		if(location.getDistance(center) <= radio) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
