package net.allegea.grepolis.api;

public abstract interface Alliance {

	public abstract int getId();
	
	public abstract void setId(int id);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract int getPoints();

	public abstract void setPoints(int points);

	public abstract int getTowns();

	public abstract void setTowns(int towns);

	public abstract int getMembers();
	
	public abstract void setMembers(int members);
	
	public abstract int getRank();
	
	public abstract void setRank(int rank);
	
}
