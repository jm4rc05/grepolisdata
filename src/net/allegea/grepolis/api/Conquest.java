package net.allegea.grepolis.api;

import java.util.Date;

public abstract interface Conquest {

	public abstract int getTownId();
	
	public abstract void setTownId(int id);
	
	public abstract Date getDate();
	
	public abstract void setDate(Date date);
	
	public abstract int getNewOwnerId();
	
	public abstract void setNewOwnerId(int id);
	
	public abstract int getNewAllianceId();
	
	public abstract void setNewAllianceId(int id);
	
	public abstract int getOldOwnerId();
	
	public abstract void setOldOwnerId(int id);
	
	public abstract int getOldAllianceId();
	
	public abstract void setOldAllianceId(int id);
	
	public abstract int getPoints();
	
	public abstract void setPoints(int points);
	
}
