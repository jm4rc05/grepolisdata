package net.allegea.grepolis.api;

public interface Kill {

	public abstract int getPosition();
	
	public abstract void setPosition(int position);

	public abstract int getId();

	public abstract void setId(int id);

	public abstract int getPoints();

	public abstract void setPoints(int points);
	
}
