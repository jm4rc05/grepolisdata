package net.allegea.grepolis.api;

import java.util.Comparator;

public class PlayersByPointsAscComparator implements Comparator<Player> {
	
	@Override
	public int compare(Player first, Player second) {
		if(first == null) {
			return -1;
		}
		else if(second == null) {
			return 1;
		}
		else {
			return first.getPoints() < second.getPoints() ? -1 : first.getPoints() > second.getPoints() ? 1 : 0;
		}
	}

}
