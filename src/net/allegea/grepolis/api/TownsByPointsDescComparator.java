package net.allegea.grepolis.api;

import java.util.Comparator;

public class TownsByPointsDescComparator implements Comparator<Town> {
	
	@Override
	public int compare(Town first, Town second) {
		if(first == null) {
			return 1;
		}
		else if(second == null) {
			return -1;
		}
		else {
			return first.getPoints() < second.getPoints() ? 1 : first.getPoints() > second.getPoints() ? -1 : 0;
		}
	}

}
