package net.allegea.grepolis.api;

import java.util.Date;
import java.util.List;


public abstract interface Grepolis {

	public abstract Player getPlayer(String filePath, int id);
	
	public abstract Alliance getAlliance(String filePath, int id);

	public abstract Town getTown(String filePath, int id);
	
	public abstract List<Town> getPlayerTowns(String filePath, int id);
	
	public abstract List<Player> getPlayersInRange(String filePath, int id, int range);
	
	public abstract List<Town> getOceanTowns(String filePath, int id);
	
	public abstract List<Town> getTownsInRange(String filePath, int id, int range);
	
	public abstract List<Conquest> getConquestsByNewPlayerId(String filePath, int playerId);
	
	public abstract List<Conquest> getConquestsByNewAlliance(String filePath, int allianceId);
	
	public abstract List<Conquest> getConquestsByOldPlayerId(String filePath, int playerId);
	
	public abstract List<Conquest> getConquestsByOldAllianceId(String filePath, int allianceId);
	
	public abstract List<Conquest> getConquestsByDateRange(String filePath, Date from, Date to);
	
	public abstract Kill getKill(String filePath, int id);
	
}
