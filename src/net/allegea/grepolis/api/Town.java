package net.allegea.grepolis.api;


public interface Town {

	public abstract int getId();
	
	public abstract void setId(int id);
	
	public abstract int getPlayerId();

	public abstract void setPlayerId(int playerId);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract int getIslandX();

	public abstract void setIslandX(int islandX);

	public abstract int getIslandY();

	public abstract void setIslandY(int islandY);
	
	public abstract int getNumberOnIsland();

	public abstract void setNumberOnIsland(int numberOnIsland);

	public abstract int getPoints();

	public abstract void setPoints(int points);

}