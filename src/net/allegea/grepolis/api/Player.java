package net.allegea.grepolis.api;

public interface Player {

	public abstract int getId();

	public abstract void setId(int id);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract int getAllianceId();

	public abstract void setAllianceId(int allianceId);

	public abstract int getPoints();

	public abstract void setPoints(int points);

	public abstract int getRank();

	public abstract void setRank(int rank);

	public abstract int getTowns();

	public abstract void setTowns(int towns);

	public abstract String toString();

}