package net.allegea.grepolis.api;

import java.util.Comparator;

public class ConquestsByPointsDescComparator implements Comparator<Conquest> {

	@Override
	public int compare(Conquest first, Conquest second) {
		if(first == null) {
			return 1;
		}
		else if(second == null) {
			return -1;
		}
		else {
			return first.getPoints() < second.getPoints() ? 1 : first.getPoints() > second.getPoints() ? -1 : 0;
		}
	}

}
