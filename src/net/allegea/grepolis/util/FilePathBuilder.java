package net.allegea.grepolis.util;

import net.allegea.grepolis.util.DataFile.DataFileId;


public class FilePathBuilder {

	public static String getPath(String path, DataFileId dataFileId) {
		return path + DataFile.getFileName(dataFileId);
	}
	
	public static String getPathZip(String path, DataFileId dataFileId) {
		return path + DataFile.getFileNameZip(dataFileId);
	}
	
}
