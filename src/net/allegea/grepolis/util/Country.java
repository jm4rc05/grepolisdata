package net.allegea.grepolis.util;

import net.allegea.grepolis.util.World.WorldId;


public final class Country {

	public static enum CountryId {
		INTERNATIONAL,
		BRAZIL,
		PORTUGAL,
	}
	
	private static String[] countryList = {
		"en",
		"br",
		"pt",
	};
	
	public static String getCountryId(CountryId countryId) {
		return countryList[countryId.ordinal()];
	}
	
	public static String getCountryServer(CountryId countryId, WorldId worldId) {
		return getCountryId(countryId).concat(new Integer(worldId.ordinal() + 1).toString().trim());
	}
	
}
