package net.allegea.grepolis.util;

public class World {

		public static enum WorldId {
			ALPHA,
			BETA,
			GAMA,
			DELTA,
			EPSILON,
			ZETA,
			ETA,
			THETA,
			IOTA,
			KAPPA,
			LAMBDA,
			MY,
			NY,
			XI,
			OMICRON,
			PI,
			RHO,
			SIGMA,
			AQUILES,
			TAU,
			UPSILON,
			PHI,
			CHI,
			PSI,
			OMEGA,
			ATENAS,
			BIZANCIO,
			CORINTO,
			HIPERBOREA,
			DELFOS,
			EFESO,
			GYTHEIO,
			HERACLIAO,
			ITACA,
			JUKTAS,
			KNOSSOS,
			LAMIA,
			MARATONA,
		}
		
		public static int getWorldId(WorldId worldId) {
			return worldId.ordinal();
		}
		
}
