package net.allegea.grepolis.util;

import net.allegea.grepolis.util.Country.CountryId;
import net.allegea.grepolis.util.DataFile.DataFileId;
import net.allegea.grepolis.util.World.WorldId;


public class URLPathBuilder {

	private static final String URL_PATH = ".grepolis.com/data/";

	private static String getBasePath(CountryId countryId, WorldId worldId) {
		return "http://" + Country.getCountryServer(countryId, worldId) + URL_PATH;
	}
	
	public static String getPath(CountryId countryId, WorldId worldId, DataFileId dataFileId) {
		return getBasePath(countryId, worldId) + DataFile.getFileName(dataFileId);
	}
	
	public static String getPathZip(CountryId countryId, WorldId worldId, DataFileId dataFileId) {
		return getBasePath(countryId, worldId) + DataFile.getFileNameZip(dataFileId);
	}
	
}
