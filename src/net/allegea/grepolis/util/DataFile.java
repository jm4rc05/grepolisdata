package net.allegea.grepolis.util;


public final class DataFile {

	public static enum DataFileId {
		PLAYERS,
		ALLIANCES,
		TOWNS,
		ISLANDS,
		PLAYER_KILLS_ALL,
		PLAYER_KILLS_ATTACK,
		PLAYER_KILLS_DEFENCE,
		ALLIANCE_KILLS_ALL,
		ALLIANCE_KILLS_ATTACK,
		ALLIANCE_KILLS_DEFENCE,
		CONQUERS,
	}
	
	private static String[] dataFileName = {
		"players.txt",
		"alliances.txt",
		"towns.txt",
		"islands.txt",
		"player_kills_all.txt",
		"player_kills_att.txt",
		"player_kills_def.txt",
		"alliance_kills_all.txt",
		"alliance_kills_att.txt",
		"alliance_kills_def.txt",
		"conquers.txt",
	};
	
	public static String getFileName(DataFileId dataFileId) {
		return dataFileName[dataFileId.ordinal()];
	}
	
	public static String getFileNameZip(DataFileId dataFileId) {
		return dataFileName[dataFileId.ordinal()] + ".gz";
	}
	
}
