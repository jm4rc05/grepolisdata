package net.allegea.grepolis.util;

public class Ocean {

	private int maxX = 0;
	
	private int maxY = 0;
	
	private int minX = 99;
	
	private int minY = 99;
	
	public Ocean(int id) {
		super();
		this.minX = (id / 10) * 100;
		this.maxX = minX + 99;
		this.minY = (id - ((id / 10) * 10)) * 100;
		this.maxY = minY + 99;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMinX() {
		return minX;
	}

	public int getMinY() {
		return minY;
	}

	@Override
	public String toString() {
		return "Ocean [maxX=" + maxX + ", maxY=" + maxY + ", minX=" + minX + ", minY=" + minY + "]";
	}
	
}
